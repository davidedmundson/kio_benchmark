#include <QtTest>

#include <KDE/KIO/ListJob>
#include <KDE/KIO/Job>


class TestKio : public QObject
{
    Q_OBJECT
private slots:    
    void query() {
        KUrl url("timeline://today");
        QBENCHMARK {
            KIO::ListJob* listJob = KIO::listDir(url);
            listJob->exec();
        }
    }
    
};


QTEST_MAIN(TestKio)

#include "test.moc"
